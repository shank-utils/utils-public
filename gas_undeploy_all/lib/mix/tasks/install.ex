defmodule Mix.Tasks.Install do
  use Mix.Task

  @shortdoc "Build and install the escript binary"

  def run(args) do
    IO.puts("inside run")
    IO.puts(args)
    res = System.cmd("mix", ["escript.build"])
    IO.inspect(res)
  end
end
