defmodule GasUndeployAll.CLI do
  def main(_args \\ []) do
    # TODO - ensure that `clasp` is installed
    current_deployment_ids()
    |> undeploy_all()

    IO.puts("Done")
  end

  defp undeploy_all([]), do: :ok

  defp undeploy_all(ids) do
    ids
    |> Enum.map(&Task.async(fn -> undeploy(&1) end))
    |> Enum.map(&Task.await(&1, 120_000))
  end

  defp undeploy(id) when is_binary(id) do
    IO.puts("> undeploying #{id}")
    System.cmd("clasp", ["undeploy", id])
    IO.puts("- undeployed #{id}")
  end

  defp current_deployment_ids() do
    IO.puts("Getting the list of current deployments")

    {lines, 0} = System.cmd("clasp", ["deployments"])

    ids =
      lines
      |> String.split("\n", trim: true)
      |> Enum.filter(&String.starts_with?(&1, "- "))
      |> Enum.drop(1)
      |> Enum.map(&String.split/1)
      |> Enum.map(&Enum.at(&1, 1))

    IO.puts("Found #{length(ids)} deployments (other than @HEAD)")

    ids
  end
end
