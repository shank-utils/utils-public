# GasUndeployAll

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `gas_undeploy_all` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gas_undeploy_all, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/gas_undeploy_all](https://hexdocs.pm/gas_undeploy_all).

